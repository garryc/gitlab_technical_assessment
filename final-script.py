#!/usr/bin/python3
import yaml
import sys

# Titles used for members
member_titles = ["pm", "pmm", "cm", "backend_engineering_manager", "frontend_engineering_manager", "support", "sets", "pdm", "ux", "uxr", "tech_writer", "tw_backup", "appsec_engineer"]

# Loads Gitlab Department group labels file. Truncated to match data_stages.yml file
dept_groups = yaml.load(open("gl_dept_group.yml"), Loader=yaml.FullLoader)

# Loads file for stages/groups
data_stages = yaml.load(open("data_stages.yml"), Loader=yaml.FullLoader)

# Returns true/false whether or not a member title is found in the group list
def member_found(title, group_list):
  if title in group_list:
    return True
  else:
    return False

# Loop through group and gather all members with titles given
def get_members_from_group(member_titles, groups):
  members = []
  for mt in member_titles:
    if member_found(mt, groups):
      if type(groups[mt]) == list:
        for ml in groups[mt]:
          members.append(ml)
      elif type(groups[mt]) == str:
        members.append(groups[mt])
      else:
        sys.exit("Undefined type")
  return members

# Get the group name from unique identifier
def get_group(dept_group):
  dg = dept_group.split("-")
  if len(dg) == 3:
    dg_return = dg[2]
  elif len(dg) == 4:
    dg_return = dg[3]
  elif len(dg) == 5:
    # Unshorten mgmt to management
    if dg[4] == 'mgmt':
      output = 'management'
    else:
      output = dg[4]
    dg_return = f'{dg[3]}_{output}'
  return dg_return

# Get the current stage name from unique identifier
def get_stage(dept_group):
  dg = dept_group.split("-")
  return dg[2]

# Main function to loop through unique identifier file and output list of all members in each stage/group
def get_group_members(member_titles, list_groups, data):
  final_list = []
  for lg in list_groups:
    sm = []
    lookup_stage = get_stage(lg)
    lookup_group = get_group(lg)
    # Checks if stage label has no group suffix
    if lookup_group == lookup_stage:
      # Check if the group is the same name as the stage
      if lookup_group in data[lookup_stage]['groups']:
        group_output = data[lookup_stage]['groups'][lookup_group]
        sm.append(get_members_from_group(member_titles, group_output))
    else:
      if lookup_group in data[lookup_stage]['groups']:
        group_output = data[lookup_stage]['groups'][lookup_group]
        sm.append(get_members_from_group(member_titles, group_output))
      else:
        # If group or member is not found, then return statement of missing value
        not_found = "No members or group not found"
        sm.append(not_found)
    if len(sm) != 0:
      stage_members = { 'group_name': lg, 'group_members': sm[0] }
      final_list.append(stage_members)
  return final_list
  
# Run! 
print(get_group_members(member_titles, dept_groups['gl_dept_group'], data_stages['stages']))
